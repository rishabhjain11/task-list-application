## Task List Application ##

This is a to-do application which runs on the console. Tasks can be added to the list for later reminder and the tasks can be removed from the list on completion. This application has been coded in Java. The project has been uploaded on Bitbucket and contains the following main files under the src folder. 

1. Application.java - contains the application class which defines the behavior of the app i.e. add, remove, print operations etc. 
2. Main.java - contains the class consisting of the main function that creates an instance of the application and accepts inputs from the user and processes the same as required.

# Steps to perform:#
1. On running the application, you will be introduced to a list of options to manipulate the task list.
2. Tasks can be added using the command: add <task>. For example, add Buy Milk
3. Completed tasks can be removed from the list by providing the task id: done <id>. For example, done 1
4. To print the pending tasks, use the command: list
5. In order to exit from the program, simple type in: quit

# Implementation: #
An arraylist is used to store the tasks. An arraylist was chosen as a data structure above a linked list or tree as it provides:
1. An O(1) insertion time.
2. An O(1) search time as we can directly remove the task at the provided index, which is better than a linked list or tree. 
3. Although the removal time is O(n) but it gives the benefit that the task ids are automatically updated since the list items are shifted to the left on any removal and the indexes get updated which has to be performed manually in a linked list.
