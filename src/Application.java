import java.util.*;

public class Application {
    private List<String> taskList;
    private int noOfTasks;

    Application(){
        taskList = new ArrayList<String>();
        noOfTasks = 0;
    }

    public void addTask(String task){
        taskList.add(task);
        noOfTasks++;
        System.out.println("Added with id #" + noOfTasks);
    }

    public void removeTask(int id){
        if(id > taskList.size()){
            System.out.println("Task ID not present");
        }
        else {
            taskList.remove(id - 1);
            noOfTasks--;
            printTaskList();
        }
    }

    public void printTaskList(){
        if(taskList.size() != 0) {
            int taskID = 1;
            for (String str : taskList) {
                System.out.println(taskID + ". " + str);
                taskID++;
            }
        }
        else{
            System.out.println("list empty");
        }
    }
}