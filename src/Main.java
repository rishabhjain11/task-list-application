import java.io.IOException;
import java.util.*;

public class Main{
    public static void main(String[] args) throws IOException{
        String input;
        int quitCheck = 1;
        String split[];
        Application app = new Application();
        System.out.println("Enter add <task> to add a task");
        System.out.println("Enter done <task ID> to remove task");
        System.out.println("Enter list to print the task list");
        System.out.println("Enter quit to exit program");
        while(quitCheck != 0) {
            Scanner sc = new Scanner(System.in);
            input = sc.nextLine();
            split = input.split(" ");

            switch(split[0]){
                case "add":
                    app.addTask(input.substring(4, input.length()));
                    break;
                case "done":
                    app.removeTask(Integer.parseInt(split[1]));
                    break;
                case "list":
                    app.printTaskList();
                    break;
                case "quit":
                    quitCheck = 0;
                    System.out.println("Exiting ..");
                    break;
                default:
                    System.out.println("Incorrect Input");
                    System.out.println("Enter add <task> to add a task");
                    System.out.println("Enter done <task ID> to remove task");
                    System.out.println("Enter list to print the task list");
                    System.out.println("Enter quit to exit program");
                    break;
            }
        }
    }
}